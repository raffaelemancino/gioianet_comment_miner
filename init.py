from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

import time
import csv

from tkinter import *
from tkinter.ttk import *

def start(link, file_name):
    #This example requires Selenium WebDriver 3.13 or newer
    with webdriver.Firefox() as driver:
        wait = WebDriverWait(driver, 10)
        driver.get(link)

        data = []
        pages_num = len(driver.find_elements_by_css_selector("div#nav-bottom span"))
        for n in range(pages_num):
            pages = driver.find_elements_by_css_selector("div#nav-bottom span")
            pages[n].click()
            time.sleep(1)
            list = driver.find_elements_by_css_selector("div.rbox_m")
            for e in list:
                num = e.find_element_by_css_selector("a.comment-anchor").get_attribute('innerHTML')
                auth = e.find_element_by_css_selector("span.comment-author").get_attribute('innerHTML')
                date = e.find_element_by_css_selector("span.comment-date").get_attribute('innerHTML')
                comment = e.find_element_by_css_selector("div.comment-body").get_attribute('innerHTML')
                data.append([num, auth, date, comment])

        if pages_num==0:
            list = driver.find_elements_by_css_selector("div.rbox_m")
            for e in list:
                num = e.find_element_by_css_selector("a.comment-anchor").get_attribute('innerHTML')
                auth = e.find_element_by_css_selector("span.comment-author").get_attribute('innerHTML')
                date = e.find_element_by_css_selector("span.comment-date").get_attribute('innerHTML')
                comment = e.find_element_by_css_selector("div.comment-body").get_attribute('innerHTML')
                data.append([num, auth, date, comment])


        with open(file_name+'.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["Anchor","Author","Date", "Comment"])
            writer.writerows(data)

def Ok():
    link = t1.get()
    csv = t2.get()
    if (link!='' and csv!=''):
        main.destroy()
        start(link, csv)

main = Tk()
l1 = Label(main, text="Link")
t1 = Entry(main)
l2 = Label(main, text="Nome file destinazione")
t2 = Entry(main)
b = Button(main, text ="Ok", command = Ok)

l1.pack()
t1.pack()
l2.pack()
t2.pack()
b.pack()
main.mainloop()
